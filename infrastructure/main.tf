resource "aws_instance" "api-pessoa" {
  ami           = var.ami //AWS Linux AMI
  instance_type = var.instance_type


  tags = {
    Name = var.name_tag,
  }
}

