variable "ami" {
  type        = string
  description = "AWS Linux AMI ID in sa-east-1 Region"
  default     = "ami-01f451f00dae38302"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "API Pessoa"
}


