FROM openjdk:17

ARG PROFILE
ARG ADDITIONAL_OPTS

ENV PROFILE=${PROFILE}
ENV ADDITIONAL_OPTS=${ADDITIONAL_OPTS}


WORKDIR /opt/spring_boot

COPY /target/spring-boot*.jar spring_boot_com_mysql.jar

SHELL ["/bin/sh","-c"]

EXPOSE 8080
EXPOSE 5005
EXPOSE 3306

CMD java ${ADDITIONAL_OPTS} -jar spring_boot_com_mysql.jar spring.profiles.active=${PROFILE}

ENV PAPERTRAIL_HOST=${PAPERTRAIL_HOST}
ENV PAPERTRAIL_PORT=${PAPERTRAIL_PORT}