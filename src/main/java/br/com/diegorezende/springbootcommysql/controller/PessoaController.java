package br.com.diegorezende.springbootcommysql.controller;

import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRq;
import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRs;
import br.com.diegorezende.springbootcommysql.service.PessoaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/pessoa")
public class PessoaController {

    private final PessoaService pessoaService;

    private static final Logger log = LoggerFactory.getLogger(PessoaController.class);

    public PessoaController(PessoaService pessoaService) {
        this.pessoaService = pessoaService;
    }

    @GetMapping("/")
    public List<PessoaRs> findAll() {
        log.info("INFO - PersonController.findAll executado");
        return pessoaService.findAll();
    }

    @GetMapping("/{id}")
    public PessoaRs findById(@PathVariable("id") Long id) throws Exception{
        log.info("INFO - PersonController.findById executado");
        return pessoaService.findById(id);
    }

    @PostMapping("/")
    public void savePerson(@RequestBody PessoaRq pessoa) throws IllegalArgumentException {
        log.info("INFO - PersonController.savePerson executado");
        pessoaService.savePerson(pessoa);
    }

    @PutMapping("/{id}")
    public void updatePerson(@PathVariable("id") Long id, @RequestBody PessoaRq pessoa) throws Exception {
        log.info("INFO - PersonController.updatePerson executado");
        pessoaService.updatePerson(id,pessoa);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable("id") Long id) throws Exception {
        log.info("INFO - PersonController.deletePerson executado");
        pessoaService.deletePerson(id);
    }




}

















