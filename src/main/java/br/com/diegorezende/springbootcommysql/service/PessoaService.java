package br.com.diegorezende.springbootcommysql.service;

import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRq;
import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRs;
import br.com.diegorezende.springbootcommysql.model.Pessoa;
import br.com.diegorezende.springbootcommysql.repository.PessoaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PessoaService {

    private final PessoaRepository pessoaRepository;

    public PessoaService(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    private static final Logger log = LoggerFactory.getLogger(PessoaService.class);

    public List<PessoaRs> findAll() {
        var pessoas = pessoaRepository.findAll();
        if(pessoas.isEmpty()){
            log.debug("DEBUG - Nao existem pessoas cadastradas");
        }
        log.info("INFO - findAll() executado com sucesso");
        return pessoas
                .stream()
                .map(PessoaRs::converter)
                .collect(Collectors.toList());
    }

    public PessoaRs findById(Long id) throws Exception{
        var pessoa = pessoaRepository.getReferenceById(id);
        if(pessoa.getId()!=null){
            log.info("INFO - findById(Long id)  executado com sucesso");
            return PessoaRs.converter(pessoa);
        } else{
            log.debug("DEBUG - Id nao existente");
            throw new Exception("Id nao existente");
        }

    }


    public Pessoa savePerson(PessoaRq pessoa) throws IllegalArgumentException {

        if (pessoa.getNome() == null || pessoa.getSobrenome() == null) {
            log.debug("DEBUG - Nome e sobrenome nao podem ser nulos");
            throw new IllegalArgumentException("Nome e sobrenome nao podem ser nulos");
        }

        var p = new Pessoa();
        p.setNome(pessoa.getNome());
        p.setSobrenome(pessoa.getSobrenome());
        log.info("INFO - savePerson(PessoaRq pessoa)  executado com sucesso");
        return pessoaRepository.save(p);

    }

    public void updatePerson(Long id, PessoaRq pessoa) throws Exception {
        Optional<Pessoa> pessoaParaUpdate = pessoaRepository.findById(id);
        if (pessoaParaUpdate.isPresent()) {
            var pessoaSave = pessoaParaUpdate.get();
            pessoaSave.setNome(pessoa.getNome());
            pessoaSave.setSobrenome(pessoa.getSobrenome());
            pessoaRepository.save(pessoaSave);
            log.info("INFO - updatePerson(Long id, PessoaRq pessoa)  executado com sucesso");
        } else {
            log.debug("DEBUG - Pessoa nao encontrada");
            throw new Exception("Pessoa nao encontrada");
        }
    }

    public void deletePerson(Long id) throws Exception {
        var pessoaToDelete = pessoaRepository.findById(id);
        if (pessoaToDelete.isPresent()) {
            var pessoaDelete = pessoaToDelete.get();
            pessoaRepository.delete(pessoaDelete);
            log.info("INFO - deletePerson(Long id)  executado com sucesso");

        } else {
            log.debug("DEBUG - Pessoa para apagar nao Encontrada");
            throw new Exception("Pessoa para apagar nao Encontrada");
        }
    }
}
